import java.util.Scanner;
public class TicTacToeGame 
{
	public static void main(String[] args) 
	{
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to TicTacToe !");
		System.out.println("Player 1's token: " + Square.X);
		System.out.println("Player 2's token: " + Square.O);
			Board board = new Board();
			boolean gameOver = false;
			int player = 1;
			Square playerToken = Square.X;
			
	while (gameOver != true) 
	{
			System.out.println();
			System.out.println("Current board:");
			System.out.println(board);
		if (player == 1) 
		{
			playerToken = Square.X;
		}
		else if (player > 1)
		{
			playerToken = Square.O;
		}
			System.out.println("Player " + player + ": enter row");
					int row = reader.nextInt();
			System.out.println("Now, enter column");
					int col = reader.nextInt();
			boolean placeToken = board.placeToken(row, col, playerToken);
		while (placeToken != true) 
		{
			System.out.println("Re-enter inputs please");
			System.out.println("Enter row");
					row = reader.nextInt();
			System.out.println("Enter column");
					col = reader.nextInt();
			placeToken = board.placeToken(row, col, playerToken);
		}
			boolean win = board.checkIfWinning(playerToken);
			boolean full = board.checkIfFull();
		if (win == true) 
		{
			System.out.println();
			System.out.println(board);
			System.out.println("Player " + player + " is the winner!");
			gameOver = true;
		}	
		else if (full == true) 
		{
			System.out.println("It's a tie!");
			gameOver = true;
		}
		else 
		{
			player++;
		if (player > 2) 
		{
			player = 1;
		}
		}
		
	}	
	}
}