public class Board 
{
	private Square[][] tictactoeBoard;
	
	public Board() 
	{
		this.tictactoeBoard = new Square[3][3];
		
		for (int i = 0; i < this.tictactoeBoard.length; i++) 
		{
			for (int j = 0; j < this.tictactoeBoard[i].length; j++) 
			{
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	public boolean placeToken(int row, int col, Square playerToken) 
	{
		if (row > 2 || col > 2) 
		{
			return false;
		}
		else if (this.tictactoeBoard[row][col] != Square.BLANK) 
		{
			return false;
		}
		else 
		{
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
	}
	public boolean checkIfFull() 
	{
		boolean checkIfFull = true;
		for (int i = 0; i < this.tictactoeBoard.length; i++) 
		{
			for (int j = 0; j < this.tictactoeBoard[i].length; j++) 
			{
				if (this.tictactoeBoard[i][j] == Square.BLANK) 
				{
					checkIfFull = false;
				}
			}
		}
			return checkIfFull;
	}
	private boolean checkIfWinningHorizontal(Square playerToken) 
	{
		int num = 0;
		for (int i = 0; i < this.tictactoeBoard.length; i++) 
		{
			if (this.tictactoeBoard[i][0] == playerToken && this.tictactoeBoard[i][1] == playerToken && this.tictactoeBoard[i][2] == playerToken) 
			{
				num++;
			}
		}
			if (num == 1) 
			{	
				return true;
			}
			else 
			{
				return false;
			}
	}
	private boolean checkIfWinningVertical(Square playerToken) 
	{
		int num = 0;
		for (int i = 0; i < this.tictactoeBoard.length; i++) 
		{
			if (this.tictactoeBoard[0][i] == playerToken && this.tictactoeBoard[1][i] == playerToken && this.tictactoeBoard[2][i] == playerToken) 
			{
				num++;
			}
		}
			if (num == 1) 
			{
				return true;
			}
			else 
			{
				return false;
			}
	}
	public boolean checkIfWinning(Square playerToken) 
	{
		if (checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	public String toString() 
	{
		String s = "";
		for (int i = 0; i < this.tictactoeBoard.length; i++) 
		{
			for (int j = 0; j < this.tictactoeBoard[i].length; j++) 
			{
				s += tictactoeBoard[i][j]+" ";
			}
			s += "\n";
		}
		return s;
	}
}